#!/bin/sh
xrandr --output HDMI-0 --off --output HDMI-1 --mode 2560x1440 --pos 0x0 --rotate left --output DP-1 --off --output DP-0 --primary --mode 2560x1440 --pos 1440x352 --rotate normal
