#!/bin/sh
xrandr \
 --output HDMI-1 --primary --mode 2560x1440 --pos 0x0 --rotate normal \
 --output DP-0 --mode 2560x1440 --pos 2560x0 --rotate normal
