# Version-specific commands [grumble, grumble]
# See: https://github.com/tmux/tmux/blob/master/CHANGES
run-shell 'tmux setenv -g TMUX_VERSION $(tmux -V | \
                            sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'
set -g default-terminal "screen-256color"
set -ga terminal-overrides ",*256col*:Tc"
set-option -g set-titles on
set-option -g set-titles-string "#{session_name} - tmux"

set-option -g status-interval 5
set-option -g automatic-rename on
set-option -g automatic-rename-format '#{?#(get-gitroot "#{pane_current_path}"),#(get-gitroot "#{pane_current_path}"),#{=-15:pane_current_path}}'

# C-b is not acceptable -- Vim uses it
set-option -g prefix C-a
bind-key C-a last-window

# Start numbering at 1
set -g base-index 1

# Allows for faster key repetition
set -s escape-time 0

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# Allows us to use C-a a <command> to send commands to a TMUX session inside
# another TMUX session
bind-key a send-prefix

# Activity monitoring
setw -g monitor-activity on
set -g visual-activity on

# Vi copypaste mode
set-window-option -g mode-keys vi

# bind-key syntax changed in 2.4 -- selection / copy / paste
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.4" | bc)" = 1 ]' " \
    bind-key -t vi-copy v begin-selection; \
    bind-key -t vi-copy y copy-selection;"

if-shell -b '[ "$(echo "$TMUX_VERSION >= 2.4" | bc)" = 1 ]' " \
    bind-key -T copy-mode-vi v send-keys -X begin-selection; \
    bind-key -T copy-mode-vi y send-keys -X copy-selection;"


set-environment -g 'SSH_AUTH_SOCK' ~/.ssh/ssh_auth_sock

# reload config
bind r source-file ~/.tmux.conf \; display-message "Config reloaded..."

# ctrl + <key> for pane navigation
bind -n C-Right select-pane -R
bind -n C-Left select-pane -L
bind -n C-Up select-pane -U
bind -n C-Down select-pane -D

set -g default-command "/bin/bash"

# vim
setw -g mode-keys vi

# Prevent confirmation (y/n) on kill-
bind-key & kill-window
bind-key x kill-pane

# Open new windows/panes in the current PWD
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"

# Style changes
set -g status-style 'bg=colour236 fg=colour114 dim'
setw -g window-status-current-style 'fg=colour238 bg=colour114 bold'
setw -g window-status-current-format ' #I:#W '
setw -g window-status-style 'fg=colour114 bg=colour236'
setw -g window-status-format ' #I:#W '
set -g window-status-activity-style 'fg=colour114 bg=colour238'
set -g message-style 'fg=colour114 bg=colour238 bold'
