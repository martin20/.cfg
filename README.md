# My dotfiles #

## Install yadm ##

`git clone https://github.com/TheLocehiliosan/yadm.git ~/.yadm-project`

Install envtpl to get templating support

`pip3 install --user envtpl`

Clone the dotfiles with yadm (yadm will be on the PATH after this step)

`~/.yadm-project/yadm clone git@gitlab.com:martin20/.cfg.git`

Set the class if the default configuration is not suitable.

`yadm config local.class axis`

## Vim documentaion ##

[Vim docs](.vim/README.md)

