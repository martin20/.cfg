set nocompatible              " be iMproved, required

if !has('gui_running')
  set t_Co=256
endif

" Install vim plug if missing
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/vimplug')
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug '907th/vim-auto-save'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'itchyny/lightline.vim'
Plug 'vim-python/python-syntax'
Plug 'thomasfaingnaert/vim-lsp-snippets'
Plug 'thomasfaingnaert/vim-lsp-ultisnips'
Plug 'wsdjeg/vim-fetch'

"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<tab>"
"let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
"let g:UltiSnipsEditSplit="vertical"

" Initialize plugin system
call plug#end()

set hls             " highlight search matches
set incsearch       " move the while typing search
set smartcase       " if search contain uppercase, search is case sensitive
set ignorecase      " ignore case in searches

" Highlight columnt 80
set colorcolumn=80

" highlight trailing whitespace in red
" have this highlighting not appear whilst you are typing in insert mode
" have the highlighting of whitespace apply when you open new buffers
highlight ExtraWhitespace ctermbg=236 guibg=#e06c75
match ExtraWhitespace /\s\+$/
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=236 guibg=#e06c75
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

set cursorline      " highlight current line

if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
syntax on           " syntax highlighing
colorscheme onehalfdark
let g:lightline = {
      \ 'colorscheme': 'onehalfdark',
      \ }
" We don't need to see the mode, it is visible in lightline
set noshowmode

command WQ wq
command Wq wq
command W w
command Q q

" Custom mappings

" Set the leader key to space
let mapleader=" "
nnoremap <Space> <Nop>
set showcmd

" Fuzzy open files
nnoremap <leader><leader> :Files<CR>
nnoremap <leader><tab>    :Buffers<CR>
nnoremap <leader>fl       :Lines<CR>
nnoremap <leader>bl       :BLines<CR>
nnoremap <leader>h        :History<CR>
nnoremap <leader>a        :Commands<cr>

" Language server bindings

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    nmap <buffer> gd <plug>(lsp-peek-definition)
    nmap <buffer> gD <plug>(lsp-definition)
    nmap <buffer> <C-]> <plug>(lsp-definition)
    nmap <buffer> <leader>f <plug>(lsp-document-format)
    nmap <buffer> <leader>R <plug>(lsp-rename)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
endfunction
augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END


" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

" Remember cursor position between sessions
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4    " number of spaces to shift with << >>
set expandtab       " tabs are spaces

" always show status line
set ruler
set laststatus=2


" create backups
set backup
set writebackup
set backupdir=~/.vim/backup
set directory=~/.vim/tmp

" System clipboard on +
set clipboard=unnamed

" Reset textwidth if you've previously overridden it.
au FileType gitcommit setlocal textwidth=72

" Characters to treat as part of file names in omni complete
set isfname=@,48-57,/,.,-,_,+,,,#,%,~

" Start pyls for python files
if executable('pyls')
    " pip install python-language-server
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
        \ 'workspace_config': {'pyls': {
        \   'plugins':
        \       {'pycodestyle': {'enabled': v:false}}
        \    }
        \   }
        \ })
endif

if executable('rls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'rls',
        \ 'cmd': {server_info->['rustup', 'run', 'stable', 'rls']},
        \ 'workspace_config': {'rust': {'clippy_preference': 'on'}},
        \ 'whitelist': ['rust'],
        \ })
endif
au User lsp_setup call lsp#register_server({
    \ 'name': 'jls',
    \ 'cmd': {server_info->['/home/martinwa/wd/java-language-server/dist/lang_server_linux.sh']},
    \ 'whitelist': ['java'],
    \ })

" Auto read/write buffers when needed.
" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

set autoread

let g:vimspector_enable_mappings = 'HUMAN'

" vim-rooter:
" Change to home directory for non project files
"let g:rooter_change_directory_for_non_project_files = 'home'
" Don't nag about changing root dir
let g:rooter_silent_chdir = 1

let g:lsp_highlight_references_enabled = 1
highlight lspReference guibg=#364074
let g:lsp_diagnostics_echo_cursor = 1
let g:lsp_diagnostics_enabled = 1
let g:asyncomplete_auto_popup = 1
let g:asyncomplete_auto_completeopt = 1
let g:asyncomplete_popup_delay = 200
let g:lsp_text_edit_enabled = 1

" Auto close quick fix window
autocmd FileType qf nnoremap <buffer> <CR> <CR>:cclose<CR>

" Config for vim-auto-save plugin
let g:auto_save = 1  " enable AutoSave on Vim startup
let g:auto_save_events = ["InsertLeave", "TextChanged"]

" Keep undo history when switching buffers
set hidden
