# if running bash
export TERMINAL=alacritty

# set PATH so it includes user's private bin directories
export PATH="$HOME/bin:$HOME/local/bin:$HOME/.local/bin:$PATH"

if [ -d "$HOME/.npm-global/bin" ]; then
    export PATH="$HOME/.npm-global/bin:$PATH"
fi
if [ -d "$HOME/.cargo/bin" ]; then
    . "$HOME/.cargo/env"
fi
if [ -d "/usr/local/go/bin" ]; then
    export PATH=$PATH:/usr/local/go/bin
fi
if [ -d "$HOME/.poetry/bin" ]; then
    export PATH="$HOME/.poetry/bin:$PATH"
fi

if [ -d "$HOME/.pyenv" ]; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
fi
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        source "$HOME/.bashrc"
    fi
fi
if [ -x "$(command -v vim)" ]; then
    export EDITOR=vim
    export GIT_EDITOR=vim
else
    export EDITOR=vi
    export GIT_EDITOR=vi
fi

if [ -d "$HOME/Android/Sdk/platform-tools/" ]; then
    export PATH="$HOME/Android/Sdk/platform-tools/:$PATH"
fi
export QT_AUTO_SCREEN_SCALE_FACTOR=1

