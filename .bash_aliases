# some more ls aliases
alias ls='ls --color=auto'
alias ll='ls -alF'
alias la='ls -A'

# Go to Git root
alias groot='cd "$(git rev-parse --show-toplevel)"'

# Docker aliases
alias docker-stop-all='docker stop $(docker ps -a -q)'
alias docker-prune='docker rm -f $(docker ps -a -q) && docker system prune -fa --volumes'
alias docker-status='docker ps --format "table{{.Status}}\t{{.Names}}"'

# Open in existing gvim
alias ovim='gvim --remote'

alias http='http --verify=/etc/ssl/certs/'

# The binary for fd is called fdfind in debian
if [ -x "$(command -v fdfind)" ]; then
    alias fd=fdfind
fi
