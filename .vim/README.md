# Vim Documentation #

This documentation aims to describe how to use custom behavior in this vim
setup.

## Bindings ##

| Key(s)            |  Action                |
|-------------------|---------------------------|
| space             | The leader key            |
| leader + leader   | Open file from git        |
| leader + r        | Open any file             |
| leader + \<tab\>  | Go to buffer              |
| leader + fl       | Find lines in buffers     |
| leader + rg       | rg search in files        |
| leader + h        | Open file from history    |
| leader + d        | Go to definition          |
| leader + .        | Show doc popover          |
| leader + f        | Format document/selection |

## Language Server ##

vim-lsp is used as plugin to handle the language server protocol.

### Python Langauge Server ###

The python langage server (pyls) is started automatically if a python file is opened. The `pyls` command needs to be on the path, install it with:

`pip install python-language-server[rope,flake8,pylint,black,autopep8,test]`
`pip install pyls-black`

## Debug Adapter Protocol ##

Vimspector is added as plugin to vim.

Example, to start a python process in debug med (example is starting pytest)

`python -m ptvsd --host localhost --wait --port 5678 -m pytest`

To attach to the process from vim do:

`call vimspector#Launch()`
