

"  	Python 2 mode
let g:python_version_2 = 0
"  	Enable all highlight options above, except for previously set.
let g:python_highlight_all = 1
"  	Highlight shebang and coding headers as comments
let g:python_highlight_file_headers_as_comments = 1
"  	Disable for slow machines
let g:python_slow_sync = 1
