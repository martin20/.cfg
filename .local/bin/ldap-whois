#!/usr/bin/env python2
import base64
import re
import shlex
import subprocess
import sys

def usage():
    print 'usage: ldap-whois NAME'

def is_authenticated():
    return subprocess.call(["klist", "-s"]) == 0


def ldap_lookup(name):
    argv = '''ldapsearch -Tx -Y GSSAPI -b "OU=Axis,DC=axis,DC=com" "(&
                                (|
                                    (sAMAccountName=%s)
                                    (givenName=%s)
                                    (sn=%s*)
                                    (cn=%s*)
                                )
                            )"''' % (name, name, name, name)
    proc = subprocess.Popen(shlex.split(argv), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    return stdout

def match(key, line):
    m = re.match('^' + key + ':: (.*)', line)
    if m:
        return base64.b64decode(m.group(1))
    m = re.match('^' + key + ': (.*)', line)
    if m:
        return m.group(1)
    return None

def parse_ldap_output(raw, keys):
    def join_broken_lines(raw):
        fixed = []
        for line in raw.split('\n'):
            if line.startswith(' '):
                fixed[-1] += line[1:]
            else:
                fixed.append(line)
        return fixed

    out = []
    for line in join_broken_lines(raw):
        for key, pretty_key in keys:
            value = match(key, line)
            if value:
                if key in ('memberOf', 'manager'):
                    value = re.match('^CN=(.*?),', value).group(1).replace('\\', '')
                if key == 'dn':
                    out.append(dict())
                if pretty_key not in out[-1]:
                    out[-1][pretty_key] = []
                out[-1][pretty_key].append(value)

    return out

def pretty_print(data, keys):
    for person in data:
        for key in keys:
            if key in person:
                values = sorted(person[key])
                print '%10s : %s' % (key, values[0])
                for i in range(1, len(values)):
                    print '             %s' % values[i]
        print

def main():
    if len(sys.argv) < 2:
        usage()
        sys.exit(1)
    if not is_authenticated():
        print "No valid kerberos ticket found. Run kinit to generate one."
        sys.exit(1)
    name = ' '.join(sys.argv[1:])

    raw = ldap_lookup(name)

    keys = []
    keys.append(('cn', 'name'))
    keys.append(('dn', 'dn'))
    keys.append(('sAMAccountName', 'username'))
    keys.append(('title', 'title'))
    keys.append(('department', 'department'))
    keys.append(('manager', 'manager'))
    keys.append(('mail', 'mail'))
    keys.append(('telephoneNumber', 'telephone'))
    keys.append(('memberOf', 'memberOf'))
    keys.append(('member', 'member'))

    data = parse_ldap_output(raw, keys)
    pretty_print(data, [pretty_key for key, pretty_key in keys])

if __name__ == '__main__':
    main()
