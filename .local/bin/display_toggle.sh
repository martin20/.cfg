#!/bin/sh

connected=$(xrandr --current | rg -c ' connected ')
active=$(xrandr --listmonitors | rg -c '\d: ')

if [ $active != $connected ]; then
    if [ $(xrandr --current | rg -q -e "HDMI.* connected") ]; then
        xrand --auto
    else
        $HOME/.screenlayout/docked.sh
    fi
else
    $HOME/.screenlayout/laptop.sh
fi
