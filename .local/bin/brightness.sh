#!/bin/bash -x
function xrandr_brightness {
    declare -A values
    case "$1" in
        +)
            values=([0.10]=0.20 [0.20]=0.30 [0.30]=0.40 [0.40]=0.50 [0.50]=0.60 [0.60]=0.70 [0.70]=0.80 [0.80]=0.90 [0.90]=1.00 [1.0]=1.0)
            ;;
        -)
            values=([0.10]=0.10 [0.20]=0.10 [0.30]=0.20 [0.40]=0.30 [0.50]=0.40 [0.60]=0.50 [0.70]=0.60 [0.80]=0.70 [0.90]=0.80 [1.0]=0.90)
            ;;
        *)
            echo "Missing +/- argument"
            exit
    esac
    BRIGHTNESS=$(stdbuf -o0 xrandr --verbose | awk '/Brightness/ { print $2; exit }')
    ACTIVE_DISPLAY=$(xrandr | grep " connected " | awk '{ print$1 }')
    xrandr --output $ACTIVE_DISPLAY --brightness ${values[$BRIGHTNESS]}
}

function intel_brightness {
    echo 'intel'
    LVL=500
    DIR=/sys/class/backlight/intel_backlight 
    MAX=$(head -n 1 "$DIR/max_brightness")
    MIN=100
    CURR=$(head -n 1 "$DIR/brightness")
    case "$1" in
        +)
            NEW=$(($CURR+$LVL))
            ;;
        -)
            NEW=$(($CURR-$LVL))
            ;;
        *)
            echo "Missing +/- argument"
            exit
    esac
    if [ "$NEW" -gt "$MAX" ]; then
        NEW=$MAX
    elif [ "$NEW" -lt "$MIN" ]; then
        NEW=$MIN
    fi
    echo $NEW > "$DIR/brightness"
}

case "${BRIGHTNESS_PROVIDER:-xrandr}" in
    xrandr)
        xrandr_brightness $1
        ;;
    intel)
        intel_brightness $1
        ;;
    *)
        echo "${BRIGHTNESS_PROVIDER} not a supported brightness implmenetation"
        exit
esac

