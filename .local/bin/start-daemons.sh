#!/bin/bash

if $(tmux has-session -t daemons &> /dev/null)
then
    exit
fi

tmux new-session -d -n "Swytcher" -s daemons swytcher
tmux new-window -t daemons  -n "Session monitor" unlock-monitor
