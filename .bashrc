# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

HISTSIZE=100000                   # big big history
HISTFILESIZE=100000               # big big history

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
else
    color_prompt=
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


if [ -f "/usr/share/git/git-prompt.sh" ];then
    source /usr/share/git/git-prompt.sh
fi

bind "set show-all-if-ambiguous on"

if [ -f ~/.fzf.bash ]; then
    source ~/.fzf.bash

    export FZF_DEFAULT_COMMAND='rg --files --no-ignore --follow'
    export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND --hidden"
fi

export LESS="-RFX"


if [ -f ~/.bashrc.local ]; then
    # Include host specific .bashrc
    source ~/.bashrc.local
fi

export WORKDIR="${HOME}/wd"
function cdgit() {
    # cd to any git found in WORKDIR
    local dir
    dir=$(fd -H -t d '.git$' ${WORKDIR} | fzf -1 -e --query="$1" | xargs dirname) && cd "${dir}"
}

if [ -d "$HOME/.nvm" ]; then
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
fi

if [ -d "$HOME/.pyenv" ]; then
    eval "$(pyenv init -)"
    eval "$($HOME/.pyenv/bin/pyenv virtualenv-init -)"
fi

export PYTHON_KEYRING_BACKEND=keyring.backends.fail.Keyring

# Optimize python builds (effects pyenv install). Will result in longer build
# times but should yield better performance
export PYTHON_CONFIGURE_OPTS='--enable-optimizations --with-lto'
export PYTHON_CFLAGS='-march=native -mtune=native'

eval "$(starship init bash)"
. "$HOME/.cargo/env"

if [ -f /usr/bin/vault ]; then
    complete -C /usr/bin/vault vault
fi
