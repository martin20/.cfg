Apt packages

cargo
cmake
dmenu
firmware-iwlwifi # Wifi HP Pavilion Power
git
i3-vm
i3pystatus
j4-dmenu-desktop
libfontconfig1-dev
libfreetype6-dev
libxcb-xfixes0-dev
network-manager
pip3
pkg-config
python3
ripgrep
fd-find
suckless-tools
tmux
x11-xserver-utils
yadm

Install Alacritty
cargo install --git https://github.com/alacritty/alacritty
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /home/martin/.cargo/bin/alacritty 50
